use mpris::PlayerFinder;

fn main() {
    let finder = PlayerFinder::new().expect("Could not connect to D-Bus");
    let players = finder.find_all().expect("Could not get players");
    for player in players.iter() {
        let _ = player.play_pause().expect("Could not play/pause");
    }
}
